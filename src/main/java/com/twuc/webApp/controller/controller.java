package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class controller {
    @RequestMapping("/api/users/{id}/books")
    public String returnBooksOfUser(@PathVariable("id") Long id){
        StringBuffer stringBuffer = new StringBuffer("The book for user ");
        stringBuffer.append(id);
        return stringBuffer.toString();
    }


    @RequestMapping("/api/segments/good")
    public String isGoodfunction(){
        return "this is good";
    }

    @RequestMapping("/api/segments/{segmentName}")
    public String isSegmentfunction(@PathVariable String segmentName){
        return "this is segmentName";
    }

    @RequestMapping("/api/test?")
    public String moreThanOneParm(){
        return "ok";
    }

    @RequestMapping("/api/test/*")
    public int test_for_wildcard_end(){
        return 1;
    }

    @RequestMapping("/api/*/test")
    public int test_for_wildcard_middle(){
        return 1;
    }

    @RequestMapping("/api/wildcard/before/*/after")
    public void example(){

    }

    @RequestMapping("/api/end/sure/*test")
    public void endIsSure(){

    }

    @RequestMapping("/api/re/{name:[a-z]}")  // 把这个不小心写成等于就GG
    public void use_re() {

    }

    // 2.8
    @RequestMapping("/api/query")
    public void query(@RequestParam("id") int id) {

    }

}
