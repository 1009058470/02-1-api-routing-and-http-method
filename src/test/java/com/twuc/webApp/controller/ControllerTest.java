package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {
    @Autowired
    MockMvc mockmvc;

    @Test
    void should_return_ok_and_content_type() throws Exception {
        mockmvc.perform(get("/api/users/2/books")).andExpect(status().isOk());
        mockmvc.perform(get("/api/users/2/books")).andExpect(content().contentType("text/plain;charset=UTF-8"));
    }

    @Test
    void test_body_values() throws Exception {
        mockmvc.perform(get("/api/users/2/books")).andExpect(content().string("The book for user 2"));
    }

    @Test
    void test_for_which_to_use() throws Exception {
        mockmvc.perform(get("/api/segments/good")).andExpect(content().string("this is good"));
        mockmvc.perform(get("/api/segments/good")).andExpect(content().string("this is segmentName")); // 正常会报错
    }

    @Test
    void test_for_more_one_parm_success() throws Exception {
        mockmvc.perform(get("/api/test1")).andExpect(status().isOk());
        mockmvc.perform(get("/api/test12")).andExpect(status().is4xxClientError()); // 正常报错 它只能匹配一个
    }

    @Test
    void test_for_wildcard_end() throws Exception { //匹配*在末尾的情况
        mockmvc.perform(get("/api/test/anything")).andExpect(status().isOk());
        mockmvc.perform(get("/api/test/22.22")).andExpect(status().isOk());
    }

    @Test
    void test_for_wildcard_middle() throws Exception {  //匹配*在中间
        mockmvc.perform(get("/api/anything/test")).andExpect(status().isOk());
    }

    @Test
    void test_for_example() throws Exception {  // /api/wildcard/before/*/after那么如果我用 /api/before/after 请求可以匹配
        mockmvc.perform(get("/api/before/after")).andExpect(status().is4xxClientError());  // 本身错 不能匹配
    }

    @Test
    void test_for_it_end_is_sure() throws Exception {
        mockmvc.perform(get("/api/end/sure/111test")).andExpect(status().isOk());
    }

    @Test
    void test_for_use_of_re() throws Exception {
        mockmvc.perform(get("/api/re/a")).andExpect(status().isOk());
        mockmvc.perform(get("/api/re/=")).andExpect(status().is4xxClientError()); // 理论上不匹配
    }

    @Test
    void test_for_query_string() throws Exception {
        mockmvc.perform(get("/api/query?id=2")).andExpect(status().isOk());
    }

    @Test
    void test_for_not_give_the_query_string() throws Exception {
        mockmvc.perform(get("/api/query")).andExpect(status().is4xxClientError()); // err->400
    }

    //2.8 第3个不会
}
